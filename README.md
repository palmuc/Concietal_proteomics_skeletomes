# Comparative proteomics of octocoral and scleractinian skeletomes and the evolution of coral calcification

Repository for the working paper 

Comparative proteomics of octocoral and scleractinian skeletomes and the evolution of coral calcification

Nicola Conci 1, Martin Lehman 2, Sergio Vargas 1, Gert Wӧrheide 1,3,4 *
1 Department of Earth and Environmen