#import relevant stuff

import sys
import logging
import Bio
import argparse
import csv
import collections
import warnings
import subprocess
import io
import pandas as pd
import itertools
import re

from itertools import groupby
from Bio import SeqIO
from Bio.SeqUtils import ProtParam

#Only required argument is a fasta file with the sequences to process.
#Optional arguments are: 1) (-s) output file from SignalP. If this file is not provided the script runs SignalP as subprocess.
#SignalP has to be isntalled. 2) (-m) a repeated target motif. 3) An aminoacid for which to compute distances. Output is a frequency table.

parser = argparse.ArgumentParser(usage='', description = ' ')
parser.add_argument('-i', dest = 'input_file', help = 'The input file with your sequences (fasta format)', required = True)
parser.add_argument('-s', dest = 'signalp_provided_file', help = 'SignalP output file with signal peptide prediction score')
parser.add_argument('-m', dest = 'target_motif', help = 'An aminoacidic motif. The program will calculate the distance between each occurrence.')
parser.add_argument('-d', dest = 'distribution', help = 'Single letter code for the amino acid for which to compute distances ')
args = parser.parse_args( )

logging.basicConfig(level=logging.INFO)
logging.basicConfig(format='%(message)s')

#Text formatting for logging messages output to the console

def non_secreted_seqs_warning(msg, *args, **kwargs):
    return '\n\n' + str(msg) + '\n\n'
warnings.formatwarning = non_secreted_seqs_warning

#Function that calculates frequency table for distances between the occurrences of a specific aminoacid.

def get_distances(sequence, aa):
    aa_indexes = [index for index, value in enumerate(sequence) if value == aa]
    aa_distances = [j-i for i, j in zip(aa_indexes[:-1], aa_indexes[1:])]
    dist_counts = collections.Counter(aa_distances)
    dist_counts.update(dict.fromkeys(set(range(1, max(aa_distances))).difference(dist_counts),int(0)))
    total = sum(dist_counts.values(),0.0)
    frequency_dictionary = {k:((v / total)*100) for k, v in dist_counts.items()}
    return frequency_dictionary

# If a signalp output file is provided, 

if args.signalp_provided_file:

    logging.info(' Reading signalp output file...\n')
    awk_command = ['awk', '$10=="Y" {print$1"_"$3}',args.signalp_provided_file]
    signalp_output = subprocess.run(awk_command,  capture_output = True, text=True)


# If no signalp output file is provided, signalp 4.1 is executed. The stdout is then
# redirected as stdin for awk.

elif not args.signalp_provided_file:

    logging.info("Running signalp...\n")
    signalp_command = ['./signalp','fasta',args.input_file]
    intermediate_output = subprocess.run(signalp_command, capture_output = True, text=True, check=True)
    awk_command = ['awk', '$10=="Y" {print $1"_"$3}']
    signalp_output = subprocess.run(awk_command, capture_output = True, text=True, input = intermediate_output.stdout)


signalp_dict = {}
secreted_counter = 0

for line in io.StringIO(signalp_output.stdout):
    secreted_counter += 1
    trimmed_line = line.rstrip()
    split_lines = trimmed_line.rsplit('_',1)
    signalp_dict[split_lines[0]] =  int(split_lines[1])



aa_percentages = []
aa_dist_frequencies = {}
input_Seq_counter = 0

logging.info('Done \n')
logging.info('Calculating amino acidic frequencies.\n')
for seqrecord in SeqIO.parse( args.input_file, 'fasta' ):
    input_Seq_counter +=1
    if seqrecord.id in signalp_dict.keys():
        for value in signalp_dict.values():
            trimmed_seq = str(seqrecord.seq[value:len(seqrecord.seq)])
            per_sequence_percentage =  {}
            per_sequence_percentage[ 'Sequence_ID' ] = seqrecord.id
            per_sequence_percentage['Length'] = len(trimmed_seq)

        if args.target_motif:

            indices = [x.start() for x in re.finditer(args.target_motif, str(trimmed_seq))]
            distances_between_motif = [(j-i)-len(args.target_motif) for i, j in zip(indices[:-1], indices[1:])]
            average_distance = sum(distances_between_motif)/len(distances_between_motif)
            per_sequence_percentage['Distance_between_motif'] = average_distance

        elif not args.target_motif:

            pass

        if args.distribution:

            aa_dist_frequencies[seqrecord.id] = get_distances(seqrecord.seq, args.distribution)

        elif not args:

            pass


        X = ProtParam.ProteinAnalysis( str( trimmed_seq ) )
        per_sequence_percentage['Isoelectric Point'] = X.isoelectric_point()
        per_sequence_percentage.update(X.get_amino_acids_percent())
        aa_percentages.append(per_sequence_percentage)


if aa_dist_frequencies:
    logging.info('Finished calculating distances between %s residues\n' % (args.distribution))

if secreted_counter > input_Seq_counter:
    logging.error('Unrecorgnized sequences in the signalp input file.'
    'Please make sure your fasta and signalp input files contain same sequences with same IDs')
    sys.exit()
elif secreted_counter < input_Seq_counter or secreted_counter == input_Seq_counter:

    if not aa_dist_frequencies:
        logging.info(' \n Done. %s out of %s provided sequences were predicted as secreted. '
        'Amino acids frequencies are available in Aminoacidic_composition.csv.\n' % (secreted_counter, input_Seq_counter))
    elif aa_dist_frequencies:
        logging.info('\n Done. %s out of %s provided sequences were predicted as secreted. '
        'Amino acids frequencies are available in Aminoacidic_composition.csv and distribution of %s'
        'in Distances.csv.\n' % (secreted_counter, input_Seq_counter, args.distribution))


# Script outputs are convered to pandas data frames and exported as csv in the current directory

aa_composition = pd.DataFrame(aa_percentages)
aa_composition.to_csv('~/Aminoacidic_composition.csv')


aa_distribution = pd.DataFrame(aa_dist_frequencies)
aa_distribution.to_csv('~/Distances.csv')
